
import {workspace, window, ExtensionContext, commands, env} from 'vscode';
import Nostr from './Nostr'

export function activate(context: ExtensionContext) {
	
	const config = workspace.getConfiguration('codestr')

	const relays = <string[]>config.get('relays')
	const privkey = <string>config.get('private_key')
	const nostr = new Nostr(privkey, relays)
	
	window.showInformationMessage(`relays: ${JSON.stringify(relays)} - pubkey: ${nostr.pubkey}`);


	context.subscriptions.push(commands.registerCommand('codestr.deploy', () => {
		nostr.deploy()
	}));

	context.subscriptions.push(commands.registerCommand('codestr.create', () => {
		nostr.create()
	}));

	context.subscriptions.push(commands.registerCommand('codestr.deployFromTree', (cmd) => {
		if (typeof cmd === 'undefined') {
			window.showErrorMessage("Please select context item from tree to use this command")
		} else {
			nostr.deploy(cmd.id, cmd.label)
		}
	}));

	context.subscriptions.push(commands.registerCommand('codestr.copyId', (cmd) => {
		if (typeof cmd === 'undefined') {
			window.showErrorMessage("Please select context item from tree to use this command")
		} else {
			env.clipboard.writeText(cmd.id).then(() => {
				window.showInformationMessage(`copied id: ${cmd.id}`)
			})
		}
	}));
}

export function deactivate() {}
