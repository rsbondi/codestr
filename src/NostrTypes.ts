export interface NostrEvent {
    id: string
    pubkey: string
    created_at: number
    kind: number
    tags: Array<Array<string|number|boolean>>
    content: string
    sig: string
}

export interface NostrApp {
  id: string
  description: string
}

export interface Relay {
  url: string
  subKey(key: string):void
  unsubKey(key: string):void
  reqFeed(params: any):void
  reqEvent(params: any):void
  reqKey(params: any):void
  publish(event: NostrEvent):void
  close():void
  readonly status: number
}

export interface Policy {
  read:boolean
  write: boolean
}

interface Callback {
  (...args: any): void
}

export interface Pool {
  relays: Relay[]
  setPrivateKey(privateKey: string):void
  addRelay(url: string, policy:Policy):Relay
  removeRelay(url:string):void
  onEvent(cb:Callback):void
  offEvent(cb:Callback):void
  onNotice(cb:Callback):void
  offNotice(cb:Callback):void
  onAttempt(cb:Callback):void
  offAttempt(cb:Callback):void
  publish(event: NostrEvent):void
  subKey(key: string):void
  unsubKey(key: string):void
  reqFeed(params: any):void
  reqEvent(params: any):void
  reqKey(params: any):void
}