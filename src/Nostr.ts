import { relayPool, getPublicKey } from 'nostr-tools'
import { window } from 'vscode';
import { NostrEvent, NostrApp } from './NostrTypes'
import NostrAppTreeProvider from './NostrTree'

const deployed = (e:NostrEvent) => {
  window.showInformationMessage(`app deployed: ${e.id}`);
}

export default class Nostr {
  pubkey: string
  pool: any
  relays: string[]
  apps: NostrApp[]
  treeProvider: NostrAppTreeProvider

  constructor(privateKey: string, relays: string[]) {
    this.pool = relayPool()
    this.pool.setPrivateKey(privateKey)
    this.pubkey = getPublicKey(privateKey)
    this.relays = relays
    this.apps = []
    this.treeProvider = new NostrAppTreeProvider(this.apps)
    this.connect()
  }

  async connect() {
    await Promise.all(this.relays.map(r => this.pool.addRelay(r, { read: true, write: true })))
    this.pool.subKey(this.pubkey)
    this.pool.onEvent((event: NostrEvent, context: string, relay: any) => {
      console.log(`got a relay with context ${context} from ${relay.url} which is already validated.`, event)
      if (event.kind === 99 && !event.tags.length) {
        this.apps.push({id: event.id, description: event.content})
        this.treeProvider.apps = this.apps
        this.treeProvider.refresh()    
      }

    })
    await this.pool.reqFeed()
    window.registerTreeDataProvider('codestrExplore', this.treeProvider);
  
  }

  create() {
    window.showInputBox({ prompt: 'Enter description' })
      .then(desc => {
        if (desc)
          this.pool.publish({
            pubkey: this.pubkey,
            created_at: Math.round(new Date().getTime() / 1000),
            kind: 99,
            content: desc
          }).then((event: NostrEvent) => {
            window.showInformationMessage(`nostr app created: ${event.id}`);
          })
          
      });
  }

  deploy(id?:string, label?:string) {
    const editor = window.activeTextEditor
    if (editor) {

      const code = editor.document.getText()
      if (this.apps.length && code) {
        let event = {
          pubkey: this.pubkey,
          created_at: Math.round(new Date().getTime() / 1000),
          kind: 99,
          content: ``,
          tags: [
            [] as any
          ]
        }
        if (id && label) {
          event.content = `deployment ${label}`
          event.tags[0] = ["c", id, code]
          this.pool.publish(event).then(deployed)
        } else {
          window.showQuickPick(this.apps.map(a => ({label: a.description, description: a.id, detail: ''})))
            .then(pick => {
              if (pick) {
                const app = this.apps.find(a => a.id === pick.description)
                if (app?.id) {
                  event.content = `deployment ${pick.label}`
                  event.tags[0] = ["c", app.id, code]
                  this.pool.publish(event).then(deployed)
                }
              }
            })
        }
      }
    }
  }
}