import { TreeDataProvider, TreeItem, Event, EventEmitter, ProviderResult } from 'vscode'
import { NostrApp } from './NostrTypes'

class NodstrAppTreeProvider implements TreeDataProvider<NostrAppNode> {
  constructor(public apps: NostrApp[]) { }
  private _onDidChangeTreeData: EventEmitter<any> = new EventEmitter<any>();
  readonly onDidChangeTreeData: Event<any> = this._onDidChangeTreeData.event;
  getTreeItem(element: NostrAppNode): TreeItem | Thenable<TreeItem> {
    return element
  }
  getChildren(element?: NostrAppNode): ProviderResult<NostrAppNode[]> {
    let apps: NostrAppNode[] = this.apps.map(a => {
      console.log('chlld', a)
      return new NostrAppNode(a.description, a.id)
    })
    return Promise.resolve(apps)
  }
  public refresh(): any {
    this._onDidChangeTreeData.fire(null);
  }
}


export class NostrAppNode extends TreeItem {
  constructor(
    public readonly label: string,
    public readonly id: string
  ) {
    super(label)
    this.tooltip = this.id
  }
}

export default NodstrAppTreeProvider
