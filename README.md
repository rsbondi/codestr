# codestr README

This is just for demonstration purposes, nostr is in the early stages and items are still pending such as the formal [specification](https://github.com/fiatjaf/nostr/pull/14) for apps on nostr​ and being able to [use in a nodejs environment](https://github.com/fiatjaf/nostr-tools/pull/5), but the future looks bright.

[video](https://youtu.be/tsY9l-WpN1U)

[repo](https://codeberg.org/rsbondi/codestr)

[nostr](https://github.com/fiatjaf/nostr)